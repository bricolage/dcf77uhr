/**********************************************************
 Copyright(C) 2006,2007 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#define _BSD_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>

#define MAX_LINE_SIZE 1024
#define MEMORY_SIZE 512
#include <libsmbus.h>

typedef unsigned char byte;

void usage(void){
	fprintf(stderr,
					"Benutzung: i2cpwm [-d deviceid/pfad] [-r] [-led1 value] [-led2 value] [-led3 value] -kommando 0x3E,0x3F,0x3D,0x30,0x31\n");
	fprintf(stderr,"deviceid: -d 19640001\nusw\n");

}

int
main(int argc, char **argv)
{
	if(argc < 2){
		usage();
		return 1;
	}

	unsigned int i2ckommando = 0;
	unsigned int readflash = 0;
	unsigned int dcfbin = 0;
	unsigned int gettime = 0;
	unsigned int i;
	//char device = "/dev/i2c-0";
	char *device = "19640001";
	//Filename = argv[1];
	//int delay = 0;
	byte recv[35];
	unsigned int recvlen;
	
	argv ++; /* skip first arg, which is our program name ... */
	while(*argv) {
		
		if(! strcmp(*argv, "-r")){
			readflash = 1;
			argv++;
			continue;
		}
		if(! strcmp(*argv, "-dcfbin")){
			dcfbin = 1;
			argv++;
			continue;
		}
		if(! strcmp(*argv, "-gettime")){
			gettime = 1;
			argv++;
			continue;
		}
		if(! strcmp(*argv, "-settime")){
			gettime = 2;
			argv++;
			continue;
		}
		
		if(! argv[1]) {
			usage();
			return 2;
		}
		if(! strcmp(*argv, "-kommando")){
			i2ckommando = (unsigned int) strtoul(argv[1], NULL, 0);
		}
		if(! strcmp(*argv, "-d"))
			device = argv[1];


		argv += 2;
	}

	/* open the i2c port */
	struct i2cdev *devhandle = smbus_open(device, 0x06);
	if(devhandle == NULL){ /* i2c_open already emitted an error message */
		fprintf(stderr,"Konnte Device nicht oeffnen\n");
		return 1;
	}
	if(i2ckommando > 0){
		struct tm *zeit;
		time_t jetzt;
		time(&jetzt);
		zeit = localtime(&jetzt);
		
		byte i2cbuf[5];
		i2cbuf[0] = i2ckommando;
		i2cbuf[1] = zeit->tm_sec;
		i2cbuf[2] = zeit->tm_min;
		i2cbuf[3] = zeit->tm_hour;
		
		if(smbus_send(devhandle, 0x40, (unsigned char *) &i2cbuf[0], sizeof(i2cbuf)) != sizeof(i2cbuf)) {
			fprintf(stderr, "couldn't successfully send data to avr.\n");
			return 1;
		}
		if(readflash == 1)
			usleep(50000);
	}
	/* dcf daten direkt auslesen */
	if(dcfbin == 1){
		if(smbus_recv(devhandle, 0x38, (unsigned char *) &recv[0], &recvlen) <= 0) {
			fprintf(stderr, "couldn't successfully recv data from avr.\n");
			return 1;
		}
		for(i=0;i<0x08;i++){
			printf("%02X ", recv[i]);
		}
		printf("\n");
		printf("jahr  : 20%02X\n",recv[7]);
		printf("monat : %02X\n",recv[6]);
		printf("wo tag: %02X\n",recv[5]);
		printf("tag   : %02X\n",recv[4]);
		printf("stunde: %02X\n",recv[3]);
		printf("minute: %02X\n",recv[2]);
		printf("flags : %02X\n",recv[1]);
		printf("Reserveantenne    R:%02X\n", recv[1] & 0x04);
		printf("Ank. Sommerzeit  A1:%02X\n", recv[1] & 0x08);
		printf("Zeitzone 1 MESZ  Z1:%02X\n", recv[1] & 0x10);
		printf("Zeitzone 2 MEZ   Z2:%02X\n", recv[1] & 0x20);
		printf("Schaltsekunde    A2:%02X\n", recv[1] & 0x40);
		printf("Zeitkodierung Start:%02X\n", recv[1] & 0x80);
	}
	if(gettime > 0){
		if(smbus_recv(devhandle, 0x20, (unsigned char *) &recv[0], &recvlen) <= 0) {
			fprintf(stderr, "couldn't successfully recv data from avr.\n");
			return 1;
		}
/*
			buf[0x0] = sek;
			buf[0x1] = min;
			buf[0x2] = stunde;
			buf[0x3] = wochentag;
			buf[0x4] = tag;
			buf[0x5] = monat;
			buf[0x6] = jahr;
*/
		printf("%i %02i.%02i.20%02i %02i:%02i:%02i\n", recv[3],recv[4],recv[5],recv[6],recv[2],recv[1],recv[0]);
		time_t jetzt;
		time(&jetzt);

		struct tm dcftm = { 0 };
		dcftm.tm_sec = recv[0];
		dcftm.tm_min = recv[1];
		dcftm.tm_hour = recv[2];
		dcftm.tm_mday = recv[4];
		dcftm.tm_mon = recv[5] - 1;
		dcftm.tm_year = recv[6] + 100;
		time_t dcftime = mktime(&dcftm);
		
		/*
		struct tm* dcftimeneu = localtime(&dcftime);
		printf("s %i\n", dcftimeneu->tm_sec);
		printf("m %i\n", dcftimeneu->tm_min);
		printf("h %i\n", dcftimeneu->tm_hour);
		printf("D %i\n", dcftimeneu->tm_mday);
		printf("M %i\n", dcftimeneu->tm_mon);
		printf("Y %i\n", dcftimeneu->tm_year);

		struct tm* timeneu = localtime(&jetzt);
		printf("s %i\n", timeneu->tm_sec);
		printf("m %i\n", timeneu->tm_min);
		printf("h %i\n", timeneu->tm_hour);
		printf("D %i\n", timeneu->tm_mday);
		printf("M %i\n", timeneu->tm_mon);
		printf("Y %i\n", timeneu->tm_year);
*/
		double dcfVSjetzt = difftime(dcftime, jetzt);
		printf("diff: %f\n", dcfVSjetzt);
		struct timeval dcfdiff = { 0 };
		dcfdiff.tv_sec = (long int) dcfVSjetzt;
		if(gettime > 1)
		{
			if((i = adjtime(&dcfdiff, NULL)) != 0)
				printf("Setzen schlug fehl: %i\n", i);
			
		}
	}
	if(readflash == 1){
		if(smbus_recv(devhandle, 0x44, (unsigned char *) &recv[0], &recvlen) <= 0) {
			fprintf(stderr, "couldn't successfully recv data from avr.\n");
			return 1;
		}
		for(i=0;i<0x10;i++){
			printf("%02X ", recv[i]);
		}
		printf("\n");
		for(i=0x10;i<0x20;i++){
			printf("%02X ", recv[i]);
		}
		printf("\n");
	}
	return smbus_close(devhandle);
}

