PROJECT=main
MCU=atmega8
CC=avr-gcc
I2CDEV=19640001
OBJCOPY=avr-objcopy
CFLAGS=-g -mmcu=$(MCU) -Wall -Wstrict-prototypes -Os -mcall-prologues -W

$(PROJECT).hex: $(PROJECT) i2cuhr
	$(OBJCOPY) -R .eeprom -O ihex $< $@

$(PROJECT): $(PROJECT).o
	$(CC) $(CFLAGS) -o $@ -Wl,-Map,$(PROJECT).map $<

$(PROJECT).s: $(PROJECT).c
	$(CC) $(CFLAGS) -S $<

i2cuhr: i2cuhr.c
	gcc -Wall -ggdb -o i2cuhr i2cuhr.c -lsmbus

clean:
	rm -f *.map *.hex $(PROJECT) *.o *.s *~ sort i2cuhr

load: $(PROJECT).hex
	i2cloader -d $(I2CDEV) -f $(PROJECT).hex -s F00102FF -e FF -ai2c 0x06 -smbusdelay

loadsp12: $(PROJECT).hex
	sp12 -wpf $(PROJECT).hex -P255

loaddude: $(PROJECT).hex
	avrdude -p $(MCU) -U flash:w:$(PROJECT).hex -E vcc,noreset

fuse:
# interner Takt auf 8 Mhz und C fuer Quarz aktivieren
	avrdude -p m8 -U lfuse:w:0xE4:m -U hfuse:w:0xC8:m -E vcc,noreset

