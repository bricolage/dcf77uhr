/*
 Copyright(C) 2006,2007 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#define F_CPU 8000000L
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>

/* avr-libc 1.4.x !!!! */


/*i2c vars*/
volatile uint8_t byteanzahl; //zaehler der bytes beim twi empfang
volatile uint8_t runbootload;//statuszaehler fuer den sprung zum bootloader
volatile uint8_t runbootseq[3] = {0x01,0x02,0xFF}; //startsequenz zum bootloader start
volatile uint8_t i2ckommando; //kommando das ueber i2c empfangen wurden
volatile uint8_t smbuscommand; //smbus command byte
volatile uint8_t smbuscount; //bytezaehler des smbus 
volatile uint8_t Checksum; //checksumme beim eeprom schreiben
volatile uint16_t eepaddr; //eeprom schreib-start-adresse
volatile uint8_t buf[33]; //datenbuffer des smbus
volatile uint8_t bufaddr; //zeiger (zaehler) auf bufferbyte

volatile uint8_t jahr, monat, wochentag, tag, stunde, min, sek;
volatile uint8_t timerover;

volatile uint8_t TCNT2last;
volatile uint8_t dcfsync;
volatile uint8_t dcf_sek;
/* dcf_time: dummy, flags (S,A2,Z2,Z1,A1,R,x,x), min, stunde, tag, wochentag, monat, jahr */
volatile uint8_t dcf_time[0x8];
volatile uint8_t dcf_timebyte, dcf_timeparitaet;
volatile uint8_t settimenow;

void
blink(uint8_t blinki, uint8_t puls, uint8_t pause){
	uint8_t pulstemp = puls;
	uint8_t pausetemp = pause;
	while(blinki > 0){
		pulstemp = puls;
		pausetemp = pause;
		PORTB |= _BV(PB0);
		while(pulstemp > 0){
			_delay_ms(25);
			pulstemp--;
		}
		PORTB &= ~_BV(PB0);
		while(pausetemp > 0){
			_delay_ms(25);
			pausetemp--;
		}
		blinki--;
	}
}

uint8_t
		bcd2bin(uint8_t data)
{
	return data - ((data/16) * 6);
}

void
		settime(void)
{
	sek = dcf_sek;
	min = bcd2bin(dcf_time[2]);
	stunde = bcd2bin(dcf_time[3]);
	tag = bcd2bin(dcf_time[4]);
	wochentag = bcd2bin(dcf_time[5]);
	monat = bcd2bin(dcf_time[6]);
	jahr = bcd2bin(dcf_time[7]);
}

/* Interruptroutine des TWI
 */
SIGNAL (SIG_2WIRE_SERIAL)
{
	
	if((TWSR & 0xF8) == 0x80){
		/* Datenbyte wurde empfangen
		* TWDR auslesen
		*/
		if (byteanzahl == 0){
			smbuscommand = TWDR;
		}
		if (byteanzahl == 1){
			smbuscount = TWDR;
		}
		if(smbuscommand == 0xF0 && smbuscount == 3){
			if(runbootseq[byteanzahl-2] == TWDR){
				runbootload++;
			}
			else{
				runbootload = 0;
			}
		}
		else if (smbuscommand == 0x81){
			if (byteanzahl == 0){
				bufaddr = 0;
				Checksum = 0;
			}
			else if (byteanzahl == 2){
				eepaddr = TWDR;
			}
			else if (byteanzahl == 3){
				eepaddr |= TWDR<<8;
			}
		}

		else if (smbuscommand == 0x82 && byteanzahl > 1 && smbuscount > 0){
			buf[bufaddr++] = TWDR;
			Checksum = (Checksum + TWDR) & 0xFF;
			smbuscount--;
			if(bufaddr == 16){
				eeprom_write_block(&buf, (void *) eepaddr, 16);
				bufaddr = 0;
				smbuscommand = 0;
				eepaddr+=16;
			}
		}
		else if (smbuscommand == 0x40){
			if (byteanzahl > 1){
				buf[byteanzahl-2] = TWDR;
				if (--smbuscount == 0)
					i2ckommando = buf[0];
			}
		}
		byteanzahl++;
	}
	else if((TWSR & 0xF8) == 0x60){
		/* Der Avr wurde mit seiner Adresse angesprochen  */
		byteanzahl = 0;
		runbootload = 0;
		PORTB |= _BV(PB0);
	}

	/* hier wird an den Master gesendet */
	else if((TWSR & 0xF8) == 0xA8){
		if(smbuscommand == 0x83){
			TWDR = 1;
		}
		else if(smbuscommand == 0x84){
			TWDR = 0x10;
			smbuscount = 0x00;
		}
		else if(smbuscommand == 0x44){
			TWDR = 0x20;
			smbuscount = 0x00;
		}
		else if(smbuscommand == 0x38){
			TWDR = 0x08;
			smbuscount = 0x00;
		}
		else if(smbuscommand == 0x20){
			TWDR = 0x06;
			smbuscount = 0x00;
			buf[0x0] = sek;
			buf[0x1] = min;
			buf[0x2] = stunde;
			buf[0x3] = wochentag;
			buf[0x4] = tag;
			buf[0x5] = monat;
			buf[0x6] = jahr;
		}
		else
			TWDR = 0x10; //zur demo den zaehler txbyte senden
	}
	else if((TWSR & 0xF8) == 0xB8){
		if(smbuscommand == 0x83){
			TWDR = Checksum;
		}
		else if(smbuscommand == 0x84){
			if(smbuscount < 0x10){
				TWDR = eeprom_read_byte((uint8_t *)eepaddr++);
				smbuscount++;
			}
		}
		else if(smbuscommand == 0x44){
			if(smbuscount < 0x20){
				TWDR = buf[smbuscount++];
			}
		}
		else if(smbuscommand == 0x38){
			if(smbuscount < 0x08){
				TWDR = dcf_time[smbuscount++];
			}
		}
		else if(smbuscommand == 0x20){
			if(smbuscount < 0x7){
				TWDR = buf[smbuscount++];
			}
		}
		else
			TWDR++; //zur demo den zaehler txbyte senden
		
		byteanzahl++;
	}
	else{
		PORTB &= ~_BV(PB0);
	}

	TWCR |= (1<<TWINT); //TWI wieder aktivieren

}

SIGNAL (SIG_OVERFLOW2)
{
	uint8_t daysinmonth;

	sek++;
  timerover++;
	dcf_sek++;
	if (dcf_sek == 60)
	{
		dcf_sek = 0;
		if (settimenow == 1)
		{
			settime();
			settimenow = 0;
		}
	}
	if ( sek == 60 )
	{
		min++;
		if ( min == 60 )
		{
			stunde++;
			if (stunde==24) {
				/* Day does overflow */
				if ( monat < 8 ) {
					if ( monat == 2 && ((jahr % 4 == 0 && jahr % 100 != 0)  || jahr % 400 == 0)) 
						daysinmonth = 29;
					else if ( monat == 2 )
						daysinmonth = 28;
					else
						daysinmonth = ( monat % 2 == 1) ? 31 : 30;
				}
				else
					daysinmonth = monat % 2 == 1 ? 30 : 31;
				tag++;
				if (tag > daysinmonth) {
					tag = 1;
					monat++;
					if ( monat > 12) {
						jahr++;
						monat = 1;
					}
				}
				wochentag++;
				if (wochentag > 7)
					wochentag = 1;
				stunde=0;
			}
			min=0;
		}
		sek = 0;
	}
	PORTB &= ~_BV(PB0);
}

SIGNAL (SIG_OUTPUT_COMPARE2)
{
	
	
	
}

SIGNAL (SIG_COMPARATOR)
{
	uint8_t TCNT2temp = TCNT2;
	uint16_t divtime = (TCNT2temp + timerover * 0xFF) - TCNT2last;
	if(divtime > 5){
		if((ACSR & _BV(ACO)) == 0)
		{
			if(divtime > 0x38 || divtime < 0x10)
			{
				dcfsync = 0;
				PORTB &= ~_BV(PB2);
				buf[0x09] = divtime & 0xFF;
			}
			if(dcfsync > 0 && dcfsync < 60)
			{
				switch (dcfsync)
				{
					case 1:
						dcf_timebyte = 1;
					break;
					case 22:
						dcf_timebyte = 2;
						dcf_timeparitaet = 0;
					break;
					case 29:
						dcf_time[dcf_timebyte] >>= 1;
						dcf_timebyte = 0;
					break;
					case 30:
						dcf_timebyte = 3;
						dcf_timeparitaet = 0;
					break;
					case 36:
						dcf_time[dcf_timebyte] >>= 2;
						dcf_timebyte = 0;
					break;
					case 37:
						dcf_timebyte = 4;
						dcf_timeparitaet = 0;
					break;
					case 43:
						dcf_time[dcf_timebyte] >>= 2;
						dcf_timebyte = 5;
					break;
					case 46:
						dcf_time[dcf_timebyte] >>= 5;
						dcf_timebyte = 6;
					break;
					case 51:
						dcf_time[dcf_timebyte] >>= 3;
						dcf_timebyte = 7;
					break;
					case 59:
						dcf_timebyte = 0;
					break;
				}
				if (divtime > 0x28)
				{
					dcf_timeparitaet ^= 1;
					if(dcf_timebyte != 0)
						dcf_time[dcf_timebyte] = (dcf_time[dcf_timebyte]>>1) | 0x80;
					else if(dcf_timeparitaet != 0)
						dcfsync = 0;
				}
				else
				{
					if(dcfsync == 21)
					{
						dcfsync = 0;
					}
					//neuer decode
					dcf_timeparitaet ^= 0;
					if(dcf_timebyte != 0)
						dcf_time[dcf_timebyte] >>= 1;
					else if(dcf_timeparitaet != 0)
						dcfsync = 0;
				}
				if (dcfsync == 59)
				{
					settimenow = 1;
					PORTB |= _BV(PB4);
				}
				dcfsync++;
			}
			PORTB &= ~_BV(PB1);
		}
		else
		{
			if(divtime > 0xF0 || divtime < 0xC0)
			{
				dcfsync = 0;
				PORTB &= ~_BV(PB2);
				buf[0x0A] = divtime & 0xFF;
			}
			if(divtime > 0x1C0 && divtime < 0x1F0 && dcfsync == 0 && settimenow == 0)
			{
				dcfsync = 1;
				dcf_sek = 0;
				TCNT2 = 0;
				TCNT2temp = 0;
				PORTB |= _BV(PB2);
				buf[0x08] = divtime & 0xFF;
			}
			PORTB |= _BV(PB1);
			}
		TCNT2last = TCNT2temp;
		timerover = 0;
	}
	
}


void
init_twi(void)
{
	/* INIT fuer den TWI i2c
	* hier wird die Addresse des µC festgelegt
	* (in den oberen 7 Bit, das LSB(niederwertigstes Bit)
	* steht dafür ob der µC auf einen general callreagiert
	*/
	TWCR = 0;
	
	TWAR = 0x06<<1;
	
	/* TWI Control Register, hier wird der TWI aktiviert, der Interrupt aktiviert und solche Sachen
	*/
	TWCR = _BV(TWIE) | _BV(TWEN) | _BV(TWEA);
	
	/* TWI Status Register init */
	TWSR &= 0xFC; 
	
	sei();
	TWCR |= _BV(TWINT); //TWI-Modul aktiv
}




int 
main(void)
{
	void (*bootptr)( void ) = (void *) 0x0C00; //zeiger auf Bootloaderstart 1024w
	DDRB |= _BV(PB0) | _BV(PB1) | _BV(PB2) | _BV(PB3) | _BV(PB4); //LED pin auf ausgang
	PORTB |= _BV(PB3);
	init_twi();
	runbootload = 0;
	blink(5, 3, 7);
	DDRB &= ~_BV(PB3);
	uint8_t i;
	//Timer init
	ASSR = _BV(AS2); //schalte auf timer auf async
	
	TCCR2 |= _BV(WGM20) | _BV(WGM21) | _BV(COM21) | _BV(CS20) | _BV(CS22); //clk durch 128
	while((ASSR & _BV(TCR2UB)) == 0);
		
	TCNT2 = 0; //init des Timer registers
	while((ASSR & _BV(TCN2UB)) == 0);
	OCR2 = 0xE0; //init Compare registers
	while((ASSR & _BV(OCR2UB)) == 0);
	
	TIMSK |= _BV(TOIE2) | _BV(OCIE2);

	//Sekunden Pulse ausgeben
	DDRB  |= _BV(PB3);
	
	//Analog Comparator init
	ACSR |= _BV(ACIE);
	
	
	//  DDRD |= _BV(PD5) | _BV(PD6) | _BV(PD7);
	while(1) {
		if (runbootload == 3){
			TIMSK &= ~(_BV(TOIE2)|_BV(OCIE2)); /* Int. fuer Timer ausschalten */
			ACSR &= ~_BV(ACIE);
			runbootload = 0;
			bootptr();
		}
		else if (i2ckommando == 0x3F){
			for(i=0;i<0x20;i++)
			{
				buf[i] = 0;
			}
			for(i=0;i<0x8;i++)
			{
				buf[i] = dcf_time[i];
			}
			
			buf[0x10] = TCNT2;
			buf[0x11] = sek;
			buf[0x12] = min;
			buf[0x13] = stunde;
			buf[0x14] = wochentag;
			buf[0x15] = tag;
			buf[0x16] = monat;
			buf[0x17] = jahr;
			
			buf[0x08] = dcfsync;
			i2ckommando = 0;
		}
		else if (i2ckommando == 0x38){
			TCNT2 = 0;
			sek = buf[0x01];
			min = buf[0x02];
			stunde = buf[0x03];
			i2ckommando = 0;
		}
		else if (i2ckommando == 0x30){
			for(i=0;i<0x8;i++)
			{
				dcf_time[i] = 0;
			}
			TCNT2 = 0;
			sek = 0;
			min = 0;
			stunde = 0;
			tag = 0;
			wochentag = 0;
			monat = 0;
			jahr = 0;
			
			dcf_sek = 0;
			dcfsync= 0;
			PORTB &= ~_BV(PB2);
			PORTB &= ~_BV(PB4);
			i2ckommando = 0;
		}
		/*
		if(dcfsync > 0)
			PORTB |= _BV(PB2);
		else
			PORTB &= ~_BV(PB2);
		*/
		/* twi reset bei status 0x00*/
		if(TWSR == 0x00){
			init_twi();
		}
	}
	return 0;
}

